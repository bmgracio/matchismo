//
//  AppDelegate.h
//  Matchismo
//
//  Created by Engineering on 27/03/2017.
//  Copyright © 2017 OutSystems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

