//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by Engineering on 03/05/2017.
//  Copyright © 2017 OutSystems. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()

@property (strong, nonatomic) NSMutableArray* cards; //of Card
@property (nonatomic, readwrite) NSInteger score;

@end

@implementation CardMatchingGame

- (NSMutableArray *)cards
{
    if(!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (instancetype)initWithCardCount:(NSUInteger)count usingDeck:(Deck*)deck
{
    self = [super init];
    
    if(self){
        for(int i=0; i < count; i++){
            Card* card = [deck drawRandomCard];
            if(card){
                [self.cards addObject:card];
            } else {
                self = nil;
                break;
            }
        }
    }
    
    return self;
}

static const int MISMATCH_PENALTY = 2;
static const int MATCH_BONUS = 4;
static const int COST_TO_CHOOSE = 1;

- (void)chooseCardAtIndex:(NSUInteger)index
{
    Card *card = [self cardAtIndex:index];
    
    if(!card.isMatched){
        if(card.isChosen){
            [card setChosen:NO];
        } else {
            for (Card *otherCard in self.cards){
                if(otherCard.isChosen && !otherCard.isMatched){
                    int matchScore = [card match:@[otherCard]];
                    
                    if(matchScore){
                        self.score += matchScore * MATCH_BONUS;
                        [card setMatched:YES];
                        [otherCard setMatched:YES];
                    } else {
                        self.score -= MISMATCH_PENALTY;
                        [otherCard setChosen:NO];
                    }
                    
                    break;
                }
            }
            
            self.score -= COST_TO_CHOOSE;
            [card setChosen:YES];
        }
    }
}

- (Card*)cardAtIndex:(NSUInteger)index
{
    return [self.cards objectAtIndex:index];
}

@end
