//
//  Deck.h
//  Matchismo
//
//  Created by Engineering on 27/03/2017.
//  Copyright © 2017 OutSystems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

- (void)addCard:(Card *)card atTop:(BOOL)atTop;
- (void)addCard:(Card *)card;


- (Card *)drawRandomCard;

@end
